#!/usr/bin/env python3
import re
import json
from requests import Session
from collections import defaultdict, OrderedDict
from time import sleep

s = Session()

headers = [
    {"name":"Accept","value":"*/*"},
    {"name":"Accept-Encoding","value":"gzip, deflate, br"},
    {"name":"Accept-Language","value":"en-US,en;q=0.5"},
    #{"name":"Authorization","value":"Basic dGlrMjY6a240NDQzcHQ="},
    {"name":"Connection","value":"keep-alive"},
    {"name":"Host","value":"vibory2019.sats.spb.ru"},
    #{"name":"Referer","value":"https://vibory2019.sats.spb.ru/electorals/24/"},
    {"name":"User-Agent","value":"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"}
]

import warnings
warnings.filterwarnings("ignore")

#for header in headers.split('\n'):
for header in headers:
    #key, val =  re.findall('([\w-]+):(.*)', header)[0]
    #print(key, val)
    s.headers.update({header['name']: header['value']})

response = s.get(f'https://vibory2019.sats.spb.ru/', auth=('tik26', 'kn4443pt'), verify=False)
response.raise_for_status()

#print(s.headers)


'https://vibory2019.sats.spb.ru/07/3YZ9D4NIHT8ONJ1Z9EKAKMZOUFH2HQEWL3XHVE9LGDY5HAVNW2FMGW3JDNGLZ8Y7GIWN156Y6I2BQK78VGY0ZSGVHKTFNJBP02I1/hls/'

playlist = '''
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:6
#EXT-X-MEDIA-SEQUENCE:2797
#EXTINF:6.000000,
file_201909061567762105.ts
#EXTINF:4.000000,
file_201909061567762111.ts
#EXTINF:6.000000,
file_201909061567762115.ts
#EXTINF:4.000000,
file_201909061567762121.ts
#EXTINF:6.000000,
file_201909061567762125.ts
#EXTINF:4.000000,
file_201909061567762131.ts
#EXTINF:6.000000,
file_201909061567762135.ts
#EXTINF:4.000000,
file_201909061567762141.ts
#EXTINF:6.000000,
file_201909061567762145.ts
#EXTINF:4.000000,
file_201909061567762151.ts
'''
#print()
#import sys
#sys.exit(0)
data = json.load(open('cams-spb-2019.json'))

data= {24: data['24']}
#print('ok:')
#print(' '.join(x for x in data if data[x]))

#print('missing:')
#print(' '.join(x for x in data if not data[x]))

files = ['file_201909061567762105.ts', 'file_201909061567762111.ts']
files = ['file_201909061567768030.ts',
'file_201909061567768036.ts']
files = ['file_201909061567768600.ts', 'file_201909061567768606.ts', 'file_201909061567768610.ts', 'file_201909061567768616.ts', 'file_201909061567768620.ts', 'file_201909061567768626.ts', 'file_201909061567768630.ts', 'file_201909061567768636.ts', 'file_201909061567768640.ts', 'file_201909061567768646.ts']


for uik, cams in data.items():
    url = f'https://vibory2019.sats.spb.ru/electorals/{uik}/'
    #url=
    response = s.get(url, auth=('tik26', 'kn4443pt'))
    if response.status_code == 200:
        print(f'uik {uik} ok')
        #outfile.write(response.content)
        #print(response.text)
    else:
        print(f'uik {uik} ERR: {response.status_code}')
        
    s.headers.update({'Referer': url})
    for n, cam in enumerate(cams, start=1):
        outfile = open(f'{uik}_{n}.ts', 'wb+')
        url = f'https://vibory2019.sats.spb.ru/{cam["haproxy_alias"]}/{cam["cid"]}/hls/'
        print(url)
        response = s.get(url, auth=('tik26', 'kn4443pt'))
        if response.status_code == 200:
            print(f'uik {uik} cam {n} ok')
            #outfile.write(response.content)
            #files = re.findall('^(file_.*)$', response.text, flags=re.MULTILINE)
            #print(files)
            #print(response.text)
        else:
            print(f'uik {uik} cam {n} ERR: {response.status_code}')
            continue
        for file in files:
            url = f'https://vibory2019.sats.spb.ru/{cam["haproxy_alias"]}/{cam["cid"]}/hls/{file}'
            response = s.get(url, auth=('tik26', 'kn4443pt'))
            if response.status_code == 200:
                print(f'uik {uik} cam {n} data ok {file}')
                outfile.write(response.content)
            else:
                print(f'uik {uik} cam {n} data ERR: {response.status_code} {file}')
        outfile.close()
    break
#url = 'https://vibory2019.sats.spb.ru/07/3YZ9D4NIHT8ONJ1Z9EKAKMZOUFH2HQEWL3XHVE9LGDY5HAVNW2FMGW3JDNGLZ8Y7GIWN156Y6I2BQK78VGY0ZSGVHKTFNJBP02I1/hls/file_201909061567759790.ts'

#https://vibory2019.sats.spb.ru/10/9VG1FD6MKNAQ4WZVC2WXB7U12V611NPOKHX6USVCBFAOK6QYE9E6Y2JW1FWERGRZQJQGSIUYYG80JJJVRJL4X497JRZWOXQPTGMC/hls/

