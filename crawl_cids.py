#!/usr/bin/env python3
import re
import json
from requests import Session
from collections import defaultdict, OrderedDict
from time import sleep

s = Session()
existing = json.load(open('cams-spb-2019.json'))

#cams = defaultdict(dict)
cams = OrderedDict()

headers = [
    {"name":"Accept","value":"*/*"},
    {"name":"Accept-Encoding","value":"gzip, deflate, br"},
    {"name":"Accept-Language","value":"en-US,en;q=0.5"},
    #{"name":"Authorization","value":"Basic dGlrMjY6a240NDQzcHQ="},
    {"name":"Connection","value":"keep-alive"},
    {"name":"Host","value":"vibory2019.sats.spb.ru"},
    #{"name":"Referer","value":"https://vibory2019.sats.spb.ru/electorals/24/"},
    {"name":"User-Agent","value":"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"}
]

import warnings
warnings.filterwarnings("ignore")

'''Host: vibory2019.sats.spb.ru
User-Agent: Mozilla/5.0 (X11; Linux x86_64; ) Gecko/20100101 Firefox/68.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate, br
Connection: keep-alive'''

#Authorization: Basic dGlrMjY6a240NDQzcHQ=

#for header in headers.split('\n'):
for header in headers:
    #key, val =  re.findall('([\w-]+):(.*)', header)[0]
    #print(key, val)
    s.headers.update({header['name']: header['value']})

response = s.get(f'https://vibory2019.sats.spb.ru/', auth=('tik26', 'kn4443pt'), verify=False)
response.raise_for_status()

#uiks = range(1,2321)
uiks = [24]
for uik in uiks:
    #print(uik)
    s.headers.update({'Referer': f'https://vibory2019.sats.spb.ru/electorals/{uik}/'})
    #s.get('https://vibory2019.sats.spb.ru/cameras/c/24/', auth=('tik26', 'kn4443pt'))
    response = s.get(f'https://vibory2019.sats.spb.ru/cameras/c/{uik}/', verify=False, auth=('tik26', 'kn4443pt'))
    if response.status_code != 200:
        print(f'УИК {uik} ERR: {response.status_code}')
        continue
    data = response.json()
    if not data:
        if existing.get(uik):
            # do not overwrite existing
            print(f'{uik} disappeared')
            continue
        print(f'{uik} missing')
    else:
        print(f'{uik} ok')
    cams[uik] = data
    sleep(0.05)
    
import json 
json.dump(cams, open('cams-spb-2019.json', 'w+'), indent=2)

